from wordpress_xmlrpc import Client, WordPressPost, WordPressPage
from wordpress_xmlrpc.methods.posts import NewPost
from wordpress_xmlrpc.methods import posts, media
from wordpress_xmlrpc.compat import xmlrpc_client

import os,sys,ConfigParser,argparse, mimetypes,urllib

from collections import deque

def handleCaption(config, post, responseId, responseUrl, altText, image, dirName):
	width = config.get('sizing','width')
	height = config.get('sizing','height')
	caption = config.getint('options','caption')
	linkTo = config.getint('options','link')
	if linkTo == 0:
		linkUrl = responseUrl
	elif linkTo == 1:
		linkUrl = config.get('url','url') + "/?attachment_id=" + responseId
	else:
		sys.exit("The linkTo value set in the config file is wrong")
	if caption == 1:
		post.content = post.content + '[caption id="attachment_' + responseId + '" align="aligncenter" width="' + width + '"]<a href="' + linkUrl + '"><img class="size-full wp-image-' + responseId + '" src="' + responseUrl + '" alt="' + altText + '" width="' + width + '" height="' + height + '" /></a> ' + image + '[/caption]'
		return post.content
	elif caption == 0:
		post.content = post.content + '</p> <a href="' + linkUrl + '"><img class="size-full wp-image-' + responseId + '" src="'     + responseUrl + '" alt="' + altText + '" width="' + width + '" height="' + height + '" /></a> '
		return post
	else:
		sys.exit("The caption value set in the config file is wrong")

def handleAlt(alt, image, dirName):
	if alt == 1:
		altText = image
		return altText
	elif alt == 2:
		altText = dirName
		return altText
	elif alt == 0:
		altText = ''
		return altText
	else:
		sys.exit("The alt value set in the config file is wrong")


def handleGallery(config, galleryValues, post, responseId, responseUrl, altText, image, dirName):
	width = config.get('sizing','width')
	height = config.get('sizing','height')
	caption = config.getint('options','caption')
	gallery = config.getint('options','gallery')
	if gallery == 1:
		post.content = '[gallery ids="' + ",".join(galleryValues) + '"]'
		return post
	elif gallery == 0:
		post = handleCaption(config, post, responseId, responseUrl, altText, image, dirName)
		return post
	else:
		sys.exit("The gallery value set in the config file is wrong")


def handlePage(page, post, client):
	if page == 1:
		post.id = client.call(posts.NewPost(page))
	elif page == 0:
		post.id = client.call(posts.NewPost(post))
	else:
		sys.exit("The page value set in the config file is wrong")


def handlePublished(published, post):
	if published == 1:
		post.post_status = 'publish'
		return post
	elif published == 0:
		post.post_status = 'draft'
		return post
	else:
		sys.exit("The published value set in the config file is wrong")


def handleImages(config, targetdir, dirName, client, post):
	galleryValues = deque([])
	images = os.listdir(targetdir + "/" +  dirName)
	alt = config.getint('options','altsource')
	for image in images:
		if image == '.DS_Store':
				pass
		else:
			filepath = targetdir + "/" +  dirName + "/" + image
			data = {
				'name': image,
				'type': mimetypes.guess_type(filepath)[0],
			}
			with open(filepath, 'rb') as img:
			 			data['bits'] = xmlrpc_client.Binary(img.read())
			response = client.call(media.UploadFile(data))
			altText = handleAlt(alt, image, dirName)
			post = handleGallery(config, galleryValues, post, str(response['id']), response['url'], altText, image, dirName)
	return post


def main():
	parser = argparse.ArgumentParser(description='Uploads directories into Wordpress Posts')
	parser.add_argument('config', help='path to config file')

	args = parser.parse_args()
	config = ConfigParser.RawConfigParser()

	if os.path.isfile(args.config): 
		config.read(args.config)
	else:
		sys.exit(args.config + ' does not exist, or you do not have permission to access it')
	
	page = config.getint('options','page')
	
	published = config.getint('options','published')
	connectUrl = config.get('url','url') + "/xmlrpc.php"
	client = Client(connectUrl, config.get('auth','username'), config.get('auth','password'))
	targetdir = config.get('paths','targetdir')

	for dirName in next(os.walk(targetdir))[1]:
		if dirName in('.git'):
			pass
		else:
			if page == 0:
				post = WordPressPost()
			elif page == 1:
				post = WordPressPage()
			else:
				sys.exit("The page value set in the config file is wrong")
			
			post.title = dirName
			post.content = ""	
			post = handleImages(config, targetdir, dirName, client, post)
			post = handlePublished(published, post)
			handlePage(page, post, client)


if __name__ == "__main__":
	main()

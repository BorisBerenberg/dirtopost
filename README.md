# dirtopost

dirtopost is a utility designed to convert a directory structure into a set of [Wordpress] posts.

### Requirements
  - Python 2.7
  - [python-wordpress-xmlrpc]

### Version
1.1.0

### Installation

Install python-wordpress-xmlrpc:

```sh
$ pip install python-wordpress-xmlrpc
```
Checkout this repo:
```sh
$ git clone git@bitbucket.org:bberenberg/dirtopost.git dirtopost
```
Setup your config file:
```sh
$ cd dirtopost
$ cp example.cfg yoursite.cfg
$ vi yoursite.cfg
```
Replace the appropriate values:

```cfg
[auth]
username: ExampleUserName
password: ExamplePassword

[url]
url: http://example.com/xmlrpc.php

[sizing]
width: 1280
height: 1024

[paths]
targetdir: /Path/to/parent/dir

[options]
published: 0
caption: 0
altsource: 1
page: 0
gallery: 1
```

### Usage

Start by creating a directory structure that looks like this:

* Parent Directory
	- First Album Name
		+ Image Name.jpeg
		+ Image Second Name.jpeg
	- Second Album Name
		+ Image Name.jpeg
		+ Image Second Name.jpeg
	- Third Album Name
		+ Image Name.jpeg
		+ Image Second Name.jpeg

This is important because the post and page titles and or image captions will come from the directories which are children of the Parent Directory, and the image names and or captions will come from their file names.

Execute the script by doing:

```sh
python go.py yoursite.cfg
```

### Todo's

- Page uploads broken

[wordpress]:https://wordpress.org/
[python-wordpress-xmlrpc]:http://python-wordpress-xmlrpc.readthedocs.org/en/latest/index.html
